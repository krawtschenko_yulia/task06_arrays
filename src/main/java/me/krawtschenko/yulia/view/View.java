package me.krawtschenko.yulia.view;

import java.io.Serializable;
import java.util.TreeMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    protected Scanner input;
    protected TreeMap<String, MenuItem> menu;

    public View() {
        input = new Scanner(System.in, "UTF-8");
        menu = new TreeMap<>();

        MenuItem quit = new MenuItem("q", "Quit");
        menu.put("q", quit);
    }

    public void displayMenu() {
        System.out.println("MENU");

        for (Map.Entry<String, MenuItem> option : menu.entrySet()) {
            System.out.printf("%s - %s%n", option.getValue().getKey(),
                    option.getValue().getDescription());
        }
    }

    public void awaitValidMenuOption() {
        String choice;
        do {
            choice = input.nextLine().toLowerCase();
        } while (!menu.keySet().contains(choice));

        menu.get(choice).getAction().execute();
    }

    public void addMenuItemAction(String key, MenuAction action) {
        MenuItem item = menu.get(key);
        item.linkAction(action);
        menu.put(key, item);
    }

    public void printMessage(String message) {
        System.out.println(message);
    }

    public void print(Serializable contents) {
        System.out.println(contents.toString());
    }
}
