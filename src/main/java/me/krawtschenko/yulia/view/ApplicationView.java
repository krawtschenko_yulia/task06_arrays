package me.krawtschenko.yulia.view;

import java.awt.*;

public class ApplicationView extends View{
    public ApplicationView() {
        super();

        MenuItem comparePerformance = new MenuItem("1", "Compare StringList and ArrayList performance");
        super.menu.put("1", comparePerformance);

        MenuItem demonstrateComparisons = new MenuItem("2", "Sort StringDuo's");
        super.menu.put("2", demonstrateComparisons);

        MenuItem findElementViaBinarySearch = new MenuItem("3", "Find an element via binary search");
        super.menu.put("3", findElementViaBinarySearch);

        MenuItem useDeque = new MenuItem("4", "See custom deque in use");
        super.menu.put("4", useDeque);

        MenuItem usePriorityQueue = new MenuItem("5", "See custom priority queue in use");
        super.menu.put("5", usePriorityQueue);

        MenuItem manipulateArrays = new MenuItem("6", "See array manipulations");
        super.menu.put("6", manipulateArrays);

        MenuItem showGameStatistics = new MenuItem("7", "See game statistics");
        super.menu.put("7", showGameStatistics);
    }

    public String requestSearchQuery() {
        String query = "";

        while (query.isBlank()) {
            System.out.println("Search for (e.g. \"Belgium — Brussels\"): ");
            query = input.nextLine();
        }

        return query;
    }

    public void displayFound(String found) {
        System.out.print("Found: ");
        System.out.println(found);
    }

    public void printIntArrayAsTable(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.printf("%4d: %4d%n", i, array[i]);
        }
    }
}
