package me.krawtschenko.yulia.controller;

import me.krawtschenko.yulia.model.BusinessLogic;
import me.krawtschenko.yulia.model.Deque;
import me.krawtschenko.yulia.model.PriorityQueue;
import me.krawtschenko.yulia.model.StringArray;
import me.krawtschenko.yulia.model.StringDuo;
import me.krawtschenko.yulia.model.StringDuoComparator;
import me.krawtschenko.yulia.view.ApplicationView;
import me.krawtschenko.yulia.view.MenuAction;

import java.io.IOException;
import java.util.*;

public class ApplicationController {
    private BusinessLogic model;
    private ApplicationView view;

    private MenuAction comparePerformance;
    private MenuAction demonstateComparisons;
    private MenuAction findElementViaBinarySearch;
    private MenuAction useDeque;
    private MenuAction usePriorityQueue;
    private MenuAction manipulateArrays;
    private MenuAction showGameStatistics;
    private MenuAction quit;

    public ApplicationController() {
        model = new BusinessLogic();

        view = new ApplicationView();

        comparePerformance = new MenuAction() {
            @Override
            public void execute() {
                int sampleSize = 2000;

                long customMethodTime = model.measurePerformance(
                        new StringArray(),
                        sampleSize
                );
                long builtinMethodTime = model.measurePerformance(
                        new ArrayList<String>(),
                        sampleSize
                );

                view.printMessage("Time taken to add " + sampleSize
                        + " Strings to custom StringArray: " + customMethodTime);

                view.printMessage("Time taken to add " + sampleSize
                        + " Strings to ArrayList<String>: " + builtinMethodTime);
            }
        };
        view.addMenuItemAction("1", comparePerformance);

        demonstateComparisons = new MenuAction() {
            @Override
            public void execute() {
                int sampleSize = 20;

                try {
                    StringDuo[] stringPairs = model.generatePairs(sampleSize);
                    view.print(Arrays.toString(stringPairs));

                    Arrays.sort(stringPairs);
                    view.print(Arrays.toString(stringPairs));

                    ArrayList<StringDuo> pairsOfStrings =
                            randomise(stringPairs);
                    view.print(pairsOfStrings);

                    pairsOfStrings.sort(new StringDuoComparator());
                    view.print(pairsOfStrings);

                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        };
        view.addMenuItemAction("2", demonstateComparisons);

        findElementViaBinarySearch = new MenuAction() {
            @Override
            public void execute() {
                int sampleSize = 20;
                ArrayList<StringDuo> list = new ArrayList<>();

                try {
                    StringDuo[] array = model.generatePairs(sampleSize);
                    list = new ArrayList<StringDuo>(
                            Arrays.asList(array)
                    );
                } catch (IOException e) {
                    e.printStackTrace();
                }

                list.sort(new StringDuoComparator());
                view.print(list);
                String query = view.requestSearchQuery();
                StringDuo pair = new StringDuo(query);
                int found = Collections.binarySearch(list, pair,
                        new StringDuoComparator());
                view.print(found);
            }
        };
        view.addMenuItemAction("3", findElementViaBinarySearch);

        useDeque = new MenuAction() {
            @Override
            public void execute() {
                Deque deque = new Deque<Integer>(4);

                deque.offerFirst(1);
                view.printMessage("This should be 1.");
                view.print(deque.peekFirst().toString());

                deque.offerLast(2);
                view.printMessage("This should be 2.");
                view.print(deque.peekLast().toString());

                deque.offerLast(3);
                view.printMessage("This should be 3.");
                view.print(deque.peekLast().toString());

                deque.pollFirst();
                view.printMessage("This should now be 2.");
                view.print(deque.peekFirst().toString());

                deque.pollLast();
                view.printMessage("This should now be 2, too.");
                view.print(deque.peekLast().toString());
            }
        };
        view.addMenuItemAction("4", useDeque);

        usePriorityQueue = new MenuAction() {
            @Override
            public void execute() {
                PriorityQueue priorityQueue = new PriorityQueue();

                priorityQueue.offer(1, 1);
                priorityQueue.offer(2, 0);
                view.printMessage("This should be 1.");
                view.print(priorityQueue.peek().toString());

                priorityQueue.poll();
                view.printMessage("This should be 2.");
                view.print(priorityQueue.poll().toString());
            }
        };
        view.addMenuItemAction("5", usePriorityQueue);

        manipulateArrays = new MenuAction() {
            @Override
            public void execute() {
                Object[] one = { 2, 3, 5, 1, 4};
                view.printMessage("First array: ");
                view.print(Arrays.toString(one));

                Object[] two = { 2, 6, 5, 7, 4};
                view.printMessage("Second array: ");
                view.print(Arrays.toString(two));

                Object[] three = model.elementsInBothArrays(one, two);
                view.printMessage("Elements occurring in both: ");
                view.print(Arrays.toString(three));

                Object[] four = model.elementsInOneArrayOnly(one, two);
                view.printMessage("Elements occurring in one array only: ");
                view.print(Arrays.toString(four));

                Object[] array = { 1, 2, 2, 3, 3, 3, 4, 4, 4, 4 };
                view.printMessage("Array with duplicates: ");
                view.print(Arrays.toString(array));

                HashMap<Object, Integer> occurrences = model.countOccurrences(array);
                Object[] filtered = model.removeDuplicates(array, occurrences, 2);
                view.printMessage("Array with elements occurring more than two times removed: ");
                view.print(Arrays.toString(filtered));

                Object[] input = { 1, 2, 2, 3, 3, 3, 4, 4, 4, 4, 1, 2, 2 };
                view.printMessage("Array with consecutive duplicates: ");
                view.print(Arrays.toString(input));

                Object[] output = model.removeConsecutiveDuplicates(input);
                view.printMessage("Array with consecutive duplicates removed: ");
                view.print(Arrays.toString(output));
            }
        };
        view.addMenuItemAction("6", manipulateArrays);

        showGameStatistics = new MenuAction() {
            @Override
            public void execute() {
                int[] doors = model.generateArrayOfRandomNumbers(10,
                        -100, 80,
                        -5, 10);
                view.printIntArrayAsTable(doors);
                boolean canSurvive = model.calculateSumOfElements(doors) >= 0;
                if (canSurvive) {
                    int[] openDoorsOrder = model.separateIndeces(doors);
                    view.printMessage("A possible way to survive is to open" +
                            " the doors in the following order: ");
                    view.print(Arrays.toString(openDoorsOrder));
                } else {
                    view.printMessage("It is impossible to survive.");
                }
            }
        };
        view.addMenuItemAction("7", showGameStatistics);

        quit = new MenuAction() {
            @Override
            public void execute() {
                System.exit(0);
            }
        };
        view.addMenuItemAction("q", quit);
    }

    private <T> ArrayList<T> randomise(T[] array) {
        ArrayList<T> list = new ArrayList<>();
        Random rand = new Random();

        for (int i = 0; i < array.length; i++) {
            int randomIndex;
            do {
                randomIndex = rand.nextInt(array.length);
            } while (array[randomIndex] == null);

            list.add(array[randomIndex]);
            array[randomIndex] = null;
        }
        return list;
    }

    public void startMenu() {
        view.displayMenu();
        view.awaitValidMenuOption();
    }
}