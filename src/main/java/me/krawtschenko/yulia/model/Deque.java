package me.krawtschenko.yulia.model;

import java.util.ArrayList;

public class Deque<T> {
    private static int capacity = 64;
    private ArrayList<T> storage;

    public Deque() {
        storage = new ArrayList<>();
    }

    public Deque(int capacity) {
        this.capacity = capacity;
        storage = new ArrayList<>();
    }

    public boolean offerFirst(T item) {
        if (storage.size() < capacity) {
            storage.add(0, item);
            return true;
        } else {
            return false;
        }
    }

    public boolean offerLast(T item) {
        if (storage.size() < capacity) {
            storage.add(item);
            return true;
        } else {
            return false;
        }
    }

    public T pollFirst() {
        T temp = storage.get(0);
        storage.remove(0);

        return temp;
    }

    public T pollLast() {
        T temp = storage.get(storage.size() - 1);
        storage.remove(storage.size() - 1);

        return temp;
    }

    public T peekFirst() {
        return storage.get(0);
    }

    public T peekLast() {
        return storage.get(storage.size() - 1);
    }
}
