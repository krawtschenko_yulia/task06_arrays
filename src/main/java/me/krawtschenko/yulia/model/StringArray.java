package me.krawtschenko.yulia.model;

import java.util.AbstractCollection;
import java.util.ArrayList;

public class StringArray extends ArrayList {
    private String[] storage;
    private int size;
    private int nextIndex;

    public StringArray() {
        size = 1;
        storage = new String[size];
        nextIndex = 0;
    }

    public void add(String s) {
        if (nextIndex > size) {
            throw new RuntimeException("This shouldn't happen.");
        }
        if (nextIndex == size) {
            size *= 2;
            storage = new String[size];
        }
        storage[nextIndex] = s;
        nextIndex++;
    }

    public String get(int index) {
        if (index >= nextIndex) {
            throw new RuntimeException("Index out of bounds.");
        }
        return storage[index];
    }
}
