package me.krawtschenko.yulia.model;

import java.io.Serializable;

public class StringDuo implements Comparable<StringDuo>, Serializable {
    private String one;
    private String two;

    public StringDuo(String one, String two) {
        this.one = one;
        this.two = two;
    }

    public StringDuo(String dashSeparated) {
        String[] separated = dashSeparated.split("—");
        one = separated[0].trim();
        two = separated[1].trim();
    }

    @Override
    public int compareTo(StringDuo another) {
        return one.compareTo(another.one);
    }

    public String getTwo() {
        return two;
    }

    @Override
    public String toString() {
        return one + " — " + two;
    }
}
