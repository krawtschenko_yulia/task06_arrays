package me.krawtschenko.yulia.model;

import java.util.Comparator;

public class StringDuoComparator implements Comparator<StringDuo> {
    @Override
    public int compare(StringDuo some, StringDuo other) {
        return some.getTwo().compareTo(other.getTwo());
    }
}
