package me.krawtschenko.yulia.model;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;

public class BusinessLogic {
    public long measurePerformance(ArrayList array, int sampleSize) {
        long startTime = System.currentTimeMillis();

        for(int i = 0; i < sampleSize; i++) {
            array.add("hello world");
        }

        long endTime = System.currentTimeMillis();

        return endTime - startTime;
    }

    public StringDuo[] generatePairs(int number) throws IOException {
        StringDuo[] stringPairs = new StringDuo[number];
        Path source = Paths.get("countries-capitals.txt");
        Random rand = new Random();
        int randomIndex;

        try (TextFileLoader fileReader = new TextFileLoader(source)) {
            for (int i = 0; i < number; i++) {
                StringDuo newest = new StringDuo(fileReader.readLine());
                do {
                    randomIndex = rand.nextInt(number);
                } while (stringPairs[randomIndex] != null);
                stringPairs[randomIndex] = newest;
            }
        }
        return stringPairs;
    }

    public Object[] elementsInBothArrays(Object[] one, Object[] two) {
        int size = Math.max(one.length, two.length);
        Object[] three = new Object[size];
        int nextIndex = 0;

        for (int i = 0; i < one.length; i++) {
            if (isInArray(two, one[i])) {
                three[nextIndex] = one[i];
                nextIndex++;
            }
        }
        size = nextIndex;
        Object[] four = Arrays.copyOfRange(three, 0, size);

        return four;
    }

    public Object[] elementsInOneArrayOnly(Object[] one, Object[] two) {
        int size = one.length + two.length;
        Object[] three = new Object[size];
        int nextIndex = 0;

        for (int i = 0; i < one.length; i++) {
            if (!isInArray(two, one[i])) {
                three[nextIndex] = one[i];
                nextIndex++;
            }
        }
        for (int j = 0; j < two.length; j++) {
            if (!isInArray(one, two[j])) {
                three[nextIndex] = two[j];
                nextIndex++;
            }
        }
        size = nextIndex;
        Object[] four = Arrays.copyOfRange(three, 0, size);

        return four;
    }

    public HashMap<Object, Integer> countOccurrences(Object[] array) {
        HashMap<Object, Integer> occurrences = new HashMap<>();

        for (int i = 0; i < array.length; i++) {
            if (occurrences.containsKey(array[i])) {
                Integer oldValue = occurrences.get(array[i]);
                occurrences.replace(array[i], oldValue + 1);
            } else {
                occurrences.put(array[i], 1);
            }
        }

        return occurrences;
    }

    public Object[] removeDuplicates (Object[] array,
                                      HashMap<Object, Integer> occurrences,
                                      int numberOfDuplicatesAllowed) {
        Object[] filtered = new Object[array.length];
        int nextIndex = 0;

        for (int i = 0; i < array.length; i++) {
            if (occurrences.get(array[i]) <= numberOfDuplicatesAllowed) {
                filtered[nextIndex] = array[i];
                nextIndex++;
            }
        }

        Object[] trimmed = Arrays.copyOfRange(filtered, 0, nextIndex);

        return trimmed;
    }

    public Object[] removeConsecutiveDuplicates(Object[] array) {
        Object[] filtered = new Object[array.length];
        filtered[0] = array[0];
        int nextIndex = 1;
        Object lastElement = array[0];

        for (int i = 1; i < array.length; i++) {
            if (!array[i].equals(lastElement)) {
                filtered[nextIndex] = array[i];
                nextIndex++;
                lastElement = array[i];
            }
        }

        Object[] trimmed = Arrays.copyOfRange(filtered, 0, nextIndex);

        return trimmed;
    }

    public int[] generateArrayOfRandomNumbers(int size, int lowest, int highest,
                                              int lowestNotExcluded,
                                              int highestNotExcluded) {
        int[] numbers = new int[size];
        int nextIndex = 0;
        Random generator = new Random();
        int generated;

        for (int i = 0; i < size; i++) {
            do {
                generated = generator.nextInt(highest - lowest + 1);
            } while (generated + lowest > lowestNotExcluded
                    && generated + lowest < highestNotExcluded);
            numbers[nextIndex] = generated + lowest;
            nextIndex++;
        }

        return numbers;
    }

    public int calculateSumOfElements(int[] array) {
        int sum = 0;

        for (int element : array) {
            sum += element;
        }
        return sum;
    }

    public int[] separateIndeces(int[] array) {
        int[] indeces = new int[array.length];
        int nextPostIndex = 0;
        int nextPreIndex = array.length - 1;

        for (int i = 0; i < array.length; i++) {
            if (array[i] > 0) {
                indeces[nextPostIndex] = i;
                nextPostIndex++;
            } else {
                indeces[nextPreIndex] = i;
                nextPreIndex--;
            }
        }
        return indeces;
    }

    private boolean isInArray(Object[] array, Object element) {
        Object[] copy = array.clone();
        Arrays.sort(copy);
        int index = Arrays.binarySearch(copy, element);

        return index >= 0;
    }

}
