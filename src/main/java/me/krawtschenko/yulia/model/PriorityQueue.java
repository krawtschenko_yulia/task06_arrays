package me.krawtschenko.yulia.model;
import java.util.AbstractQueue;
import java.util.HashMap;
import java.util.Iterator;

public class PriorityQueue<T extends Object> extends AbstractQueue {
    private HashMap<Integer, T> storage;
    private int size;
    private int lastIndex;
    private int maxPriority;

    public PriorityQueue() {
        storage = new HashMap<>();
    }

    public Iterator iterator() {
        return null;
    }

    public int size() {
        return size;
    }

    public boolean offer(Object item, int priority) {
        if (storage.containsValue(item)) {
            return false;
        }
        storage.put(priority, (T) item);
        if (priority > maxPriority) {
            maxPriority = priority;
        }
        return true;
    }

    public boolean offer(Object item) {
        if (storage.containsValue(item)) {
            return false;
        }
        storage.put(0, (T) item);
        maxPriority = 0;

        return true;
    }

    public T poll() {
        T head = storage.get(maxPriority);
        int headPriority = maxPriority;
        maxPriority = 0;
        for(Integer priority : storage.keySet()) {
            if (priority > maxPriority) {
                maxPriority = priority;
            }
        }
        storage.remove(headPriority);
        findMaxPriority();

        return head;
    }

    public Object peek() {
        return storage.get(maxPriority);
    }

    public boolean isEmpty() {
        return storage.isEmpty();
    }

    private void findMaxPriority() {
        maxPriority = 0;
        for (Integer key : storage.keySet()) {
            if (key > maxPriority) {
                maxPriority = key;
            }
        }
    }
}
