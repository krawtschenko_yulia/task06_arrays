package me.krawtschenko.yulia.model;

import me.krawtschenko.yulia.model.Book;

public class Library<T> {
    private Book[] storage;
    private int size;
    private int lastIndex;

    public Library() {
        storage = new Book[size];
        size = 2;
        lastIndex = 0;
    }

    public void add(T item) throws Exception {
        if (lastIndex > size) {
            throw new Exception("This shouldn't happen.");
        }
        if (lastIndex == size) {
            size *= 2;
            storage = new Book[size];
        }
        storage[lastIndex] = (Book) item;
        lastIndex++;
    }
}
